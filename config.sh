#!/bin/bash

for i in *fedora.config ; do
    # do something with $file
	echo 'CONFIG_ZENIFY=y' >> $i
	echo 'CONFIG_SCHED_PDS=y' >> $i
	echo '# CONFIG_CC_OPTIMIZE_FOR_PERFORMANCE_O3 is not set' >> $i
	echo '# CONFIG_HZ_750 is not set' >> $i
	echo 'CONFIG_NR_TTY_DEVICES=63' >> $i
	echo 'CONFIG_RAID6_USE_PREFER_GEN=y' >> $i
	echo '# CONFIG_MK8SSE3 is not set' >> $i
	echo '# CONFIG_MK10 is not set' >> $i
	echo '# CONFIG_MBARCELONA is not set' >> $i
	echo '# CONFIG_MBOBCAT is not set' >> $i
	echo '# CONFIG_MJAGUAR is not set' >> $i
	echo '# CONFIG_MBULLDOZER is not set' >> $i
	echo '# CONFIG_MPILEDRIVER is not set' >> $i
	echo '# CONFIG_MSTEAMROLLER is not set' >> $i
	echo '# CONFIG_MEXCAVATOR is not set' >> $i
	echo '# CONFIG_MZEN is not set' >> $i
	echo '# CONFIG_MZEN2 is not set' >> $i
	echo '# CONFIG_MNEHALEM is not set' >> $i
	echo '# CONFIG_MWESTMERE is not set' >> $i
	echo '# CONFIG_MSILVERMONT is not set' >> $i
	echo '# CONFIG_MSANDYBRIDGE is not set' >> $i
	echo '# CONFIG_MIVYBRIDGE is not set' >> $i
	echo '# CONFIG_MHASWELL is not set' >> $i
	echo '# CONFIG_MBROADWELL is not set' >> $i
	echo '# CONFIG_MSKYLAKE is not set' >> $i
	echo '# CONFIG_MSKYLAKEX is not set' >> $i
	echo '# CONFIG_MCANNONLAKE is not set' >> $i
	echo '# CONFIG_MICELAKE is not set' >> $i
	echo '# CONFIG_MNATIVE is not set' >> $i
	echo 'CONFIG_SMT_NICE=y' >> $i
	echo '# CONFIG_TP_SMAPI is not set' >> $i
done
